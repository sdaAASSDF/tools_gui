package server

import (
	"fmt"
	rice "github.com/GeertJohan/go.rice"
	"github.com/foolin/goview"
	"github.com/foolin/goview/supports/ginview"
	"github.com/foolin/goview/supports/gorice"
	"github.com/gin-gonic/gin"
	"net/http"
	"seo_tools/db"
	"seo_tools/server/controller"
	"seo_tools/server/view_func"
)

func Server(port int) {
	r := gin.Default()

	debug := db.Debug()

	/* 处理静态文件 */

	if debug {

		r.Static("/static", "./server/z_static")
	} else {
		box := rice.MustFindBox("z_static")
		cssFileServer := http.StripPrefix("/static", http.FileServer(box.HTTPBox()))
		r.GET("/static/:a", gin.WrapH(cssFileServer))
	}

	tempFunc := view_func.TempFunc()
	tempFunc["h"] = func() string {
		h := "http://127.0.0.1:" + fmt.Sprintf("%d", port)
		return h
	}

	//模板文件
	if debug {
		_view_config := goview.DefaultConfig
		_view_config.Root = "server/z_view"
		_view_config.Funcs = tempFunc
		_view_config.DisableCache = true

		r.HTMLRender = ginview.New(_view_config)

	} else {
		basic := gorice.NewWithConfig(rice.MustFindBox("z_view"), goview.Config{
			Root:         "z_view",
			Extension:    ".html",
			Master:       "layouts/master",
			Funcs:        tempFunc,
			DisableCache: true,
		})
		r.HTMLRender = ginview.Wrap(basic)
	}

	r.Use(controller.SSS())

	r.GET("/", controller.Home)

	for _, v := range controller.Nav() {
		if v.U != "" {
			r.GET(v.U, v.F)
		}
		for _, v2 := range v.Child {
			if v2.U == "" {
				continue
			}
			r.GET(v2.U, v2.F)
		}
	}

	r.Run(fmt.Sprintf("127.0.0.1:%d", port))

}
