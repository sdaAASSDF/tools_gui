package controller

import "github.com/gin-gonic/gin"

func pm_baidu_pc(c *gin.Context) {

	_ht := defaultData(c)
	_ht["title"] = "百度PC端排名查询"
	Render(c, "pm_baidu_pc", _ht)
}

func pm_baidu_m(c *gin.Context) {
	_ht := defaultData(c)
	_ht["title"] = "百度移动端排名查询"
	Render(c, "pm_baidu_m", _ht)
}
