package controller

import (
	"github.com/gin-gonic/gin"
	"seo_tools/db"
	"seo_tools/verifyauth"
	"time"
)

func Version() string {
	return verifyauth.Version()
}
func Title() string {
	return "梭哈SEO工具"
}

func defaultData(c *gin.Context) gin.H {

	nav := Nav()
	for k, _ := range nav {
		if nav[k].U == "" {
			nav[k].U = "/"
		}
	}

	_ht := gin.H{
		"t":     Title() + " - " + Version(),
		"nav":   nav,
		"_path": c.Request.URL.Path,
		"time":  time.Now().UnixNano(),
		"debug": db.Debug(),
	}
	return _ht
}

func Render(c *gin.Context, tpl_name string, _ht gin.H) {
	c.HTML(200, tpl_name, _ht)
}
