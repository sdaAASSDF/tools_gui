package controller

import (
	"github.com/gin-gonic/gin"
	"seo_tools/db"
)

func SSS() gin.HandlerFunc {
	return func(c *gin.Context) {
		SQMCODEISOK := db.SQMGET()
		if !SQMCODEISOK {
			if c.Request.URL.Path != "/error" && c.Request.URL.Path != "/check_error.html" {
				c.Redirect(301, "/error?message=请勿越界")
				c.Abort()
			}
		}

	}
}

func Error(c *gin.Context) {

	_ht := defaultData(c)
	sqm_str := db.AUTHGET()
	sqm_status := db.SQMGET()
	message := c.DefaultQuery("message", "无效验证码")
	_ht["err_str"] = message
	if sqm_status {
		_ht["err_str"] = "正常使用"
	}
	_ht["sqm_str"] = sqm_str
	Render(c, "error", _ht)
}
