package controller

import (
	"github.com/gin-gonic/gin"
)

type NNN struct {
	T     string
	U     string
	F     (func(ctx *gin.Context))
	Child []NNN
}

func Nav() []NNN {
	return []NNN{
		{
			T: "推送工具",
			U: "",
			Child: []NNN{
				{
					T:     "神马提交",
					U:     "/tj_sm.html",
					F:     TJ_SM,
					Child: nil,
				},
				{
					T:     "搜狗-链接提交",
					U:     "/tj_sg_1.html",
					F:     TJ_SG_1,
					Child: nil,
				},
				{
					T:     "360-收录提交",
					U:     "/tj_360_1.html",
					F:     TJ_360_1,
					Child: nil,
				},
				{
					T:     "百度主动提交",
					U:     "/tj_baidu_1.html",
					F:     TJ_BAIDU_1,
					Child: nil,
				},
				{
					T:     "百度手动提交",
					U:     "/tj_baidu_2.html",
					F:     TJ_BAIDU_2,
					Child: nil,
				},
				{
					T:     "百度批量添加二级域名",
					U:     "/tj_baidu_3.html",
					F:     TJ_BAIDU_3,
					Child: nil,
				},
			},
		},
		{
			T: "实时排名查询",
			U: "",
			F: nil,
			Child: []NNN{
				{
					T:     "百度PC查询",
					U:     "/pm_baidu_pc.html",
					F:     pm_baidu_pc,
					Child: nil,
				},
				{
					T:     "百度移动查询",
					U:     "/pm_baidu_m.html",
					F:     pm_baidu_m,
					Child: nil,
				},
				{
					T:     "360PC查询",
					U:     "/pm_so_pc.html",
					F:     pm_so_pc,
					Child: nil,
				},
				{
					T:     "360移动查询",
					U:     "/pm_so_m.html",
					F:     pm_so_m,
					Child: nil,
				},
			},
		},
		{
			T:     "打码平台",
			U:     "/coding_platform",
			F:     CodingPlatform,
			Child: nil,
		},
		{
			T:     "授权码",
			U:     "/error",
			F:     Error,
			Child: nil,
		},
	}
}
