package controller

import (
	"github.com/gin-gonic/gin"
)

func CodingPlatform(c *gin.Context) {
	_ht := defaultData(c)
	Render(c, "coding_platform", _ht)
}
