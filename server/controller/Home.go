package controller

import "github.com/gin-gonic/gin"

func Home(c *gin.Context) {
	_ht := defaultData(c)
	Render(c, "index", _ht)
}
