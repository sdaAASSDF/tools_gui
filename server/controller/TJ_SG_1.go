package controller

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/gin-gonic/gin"
	"seo_tools/db"
	"seo_tools/h"
	"strings"
)

//搜狗链接提交站内

//http://zhanzhang.sogou.com/index.php/sitelink/index

func TJ_SG_1(c *gin.Context) {
	_ht := defaultData(c)

	cookie := db.SGGet().Cookie
	_ht["title"] = "搜狗-链接提交"
	_ht["cookie"] = cookie

	_get, err := h.Get("http://zhanzhang.sogou.com/index.php/statistics/index", map[string]string{}, cookie)
	if err != nil {
		_ht["err"] = "提取域名列表失败,刷新重试"
	}
	user_name := _sg_get_name(_get)
	if user_name == "" {
		user_name = "未找到用户名,可能 Cookie 无效"
	}
	_ht["user_name"] = user_name
	Render(c, "tj_sg_1", _ht)
}

func _sg_get_name(content string) (user_name string) {
	reader, err := goquery.NewDocumentFromReader(strings.NewReader(content))
	if err != nil {
		return
	}
	defer reader.Clone()
	user_name = reader.Find(".user_name").Text()
	return
}
