package controller

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/gin-gonic/gin"
	"seo_tools/db"
	"seo_tools/h"
	"strings"
)

func TJ_SM(c *gin.Context) {
	_ht := defaultData(c)
	_ht["title"] = "神马MIP提交"
	sm := db.SMGet()
	//提取域名
	_get, err := h.Get("https://zhanzhang.sm.cn/open/mip", map[string]string{}, sm.Cookie)
	if err != nil {
		_ht["err"] = "提取域名列表失败,刷新重试"
	}
	list, auth := __sm_domain_list(_get)
	_ht["domain_list"] = list
	_ht["auth"] = auth
	_ht["cookie"] = sm.Cookie
	Render(c, "tj_sm", _ht)
}
func __sm_domain_list(content string) (list []map[string]string, auth string) {
	reader, err := goquery.NewDocumentFromReader(strings.NewReader(content))
	if err != nil {
		return
	}
	defer reader.Clone()
	reader.Find(".zz-title option").Each(func(i int, selection *goquery.Selection) {
		text := selection.Text()
		attr, _ := selection.Attr("data-href")
		if text != "" && attr != "" {
			list = append(list, map[string]string{
				"domain": text,
				"href":   attr,
			})
		}
	})
	auth = reader.Find(".authkey").Text()
	return
}
