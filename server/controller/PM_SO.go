package controller

import "github.com/gin-gonic/gin"

func pm_so_pc(c *gin.Context) {

	_ht := defaultData(c)
	_ht["title"] = "360 PC端排名查询"
	Render(c, "pm_so_pc", _ht)
}

func pm_so_m(c *gin.Context) {
	_ht := defaultData(c)
	_ht["title"] = "360 移动端排名查询"
	Render(c, "pm_so_m", _ht)
}
