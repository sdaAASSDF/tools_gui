package main

import (
	"github.com/gin-gonic/gin"
	"os"
	"seo_tools/api"
)

func main() {
	os.Mkdir("code/", os.ModePerm)
	r := gin.Default()
	r.Use(api.FKSS())
	r.GET("/i_ai_suoha", api.IMissSuoHa)
	r.POST("/sogou_code/4", api.SOGOUCODE4)
	//r.GET("/sqm_verify/:file",api.SqmVerify)
	r.Run(":8933")
}
