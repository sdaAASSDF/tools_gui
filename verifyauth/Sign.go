package verifyauth

import (
	"encoding/base64"
	"encoding/json"
	"github.com/wumansgy/goEncrypt"
	"seo_tools/db"
)

func Sign() []byte {
	return []byte("sdf5164szfsggdgkjbdhfsgjsdbgsdgolisdhguiosdgf")[:8]
}
func EncryptData(d interface{}) (end string, isok bool) {
	mar, err := json.Marshal(d)
	if err != nil {

		return
	}
	encrypt, err := goEncrypt.DesCbcEncrypt(mar, Sign())
	if err != nil {
		return
	}
	end = base64.StdEncoding.EncodeToString(encrypt)
	isok = true
	return
}

func DecryptData(code string) (sqm db.SQM, isok bool) {
	toString, err := base64.StdEncoding.DecodeString(code)
	if err != nil {
		return
	}
	newplaintext, err := goEncrypt.DesCbcDecrypt([]byte(toString), Sign()) //解密得到密文,可以自己传入初始化向量,如果不传就使用默认的初始化向量,8字节

	err = json.Unmarshal(newplaintext, &sqm)
	if err != nil {
		return
	}
	isok = true
	return
}

func DecryptDataAPI(code string) (sqm db.APIRESULT, isok bool) {
	toString, err := base64.StdEncoding.DecodeString(code)
	if err != nil {
		return
	}
	newplaintext, err := goEncrypt.DesCbcDecrypt([]byte(toString), Sign()) //解密得到密文,可以自己传入初始化向量,如果不传就使用默认的初始化向量,8字节

	err = json.Unmarshal(newplaintext, &sqm)
	if err != nil {
		return
	}
	isok = true
	return
}
