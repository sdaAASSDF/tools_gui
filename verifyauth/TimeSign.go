package verifyauth

import (
	"strconv"
	"time"
)

var cstZone = time.FixedZone("CST", 8*3600) // 东八
func TimeSign() int {

	_id, _ := strconv.Atoi(time.Now().In(cstZone).Format("20060102150405"))
	return _id
}

//验证时间是否正确
func TimeVerify(now_req int) bool {
	mt, _ := time.ParseDuration("-30m")
	now_time, _ := strconv.Atoi(time.Now().In(cstZone).Add(mt).Format("20060102150405"))
	if now_time == 0 || now_req == 0 {
		return false
	}
	if now_time > now_req {
		return false
	}
	return true
}
