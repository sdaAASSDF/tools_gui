package verifyauth

import "seo_tools/db"

func Version() string {
	return "v1.0.3"
}
func API(url string) string {
	_u := "http://139.155.58.169:8933"
	if db.Debug() {
		_u = "http://127.0.0.1:8933"
	}
	s := _u + url
	return s
}
