package db

import "github.com/gin-gonic/gin"

type SQM struct {
	Time    int
	Code    string
	Version string
	CPUID   string
	UUID    string
	Rand    int
}

type SQMFILE struct {
	UUID  string `json:"uuid"`
	CPUID string `json:"cpuid"`
}

type APIRESULT struct {
	Data    gin.H
	Time    int
	Version string
	Err     string
}

func AUTHGET() string {
	s := read(KAUTH)
	return s
}
func AUTHSET(sqm string) {
	insert(KAUTH, sqm)
	return
}

func SQMGET() bool {
	s := read(KSQMISOK)
	isok := false
	if s == "1" {
		isok = true
	}
	return isok
}
func SQMSET(sqm bool) {
	s := "0"
	if sqm {
		s = "1"
	}
	insert(KSQMISOK, s)
	return
}
