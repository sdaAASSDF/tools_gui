package db

var debug bool

func SetDebug(d bool) {
	debug = d
}
func Debug() bool {
	return debug
}
