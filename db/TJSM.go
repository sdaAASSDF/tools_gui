package db

import "encoding/json"

type TJSM struct {
	Cookie string
}

func SMSave(data TJSM) {
	marshal, err := json.Marshal(data)
	if err != nil {
		return
	}
	insert(KTJSM, string(marshal))
}

func SMGet() (s TJSM) {
	s2 := read(KTJSM)
	json.Unmarshal([]byte(s2), &s)
	return
}
