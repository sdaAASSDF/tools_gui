package db

import "encoding/json"

type TJSG struct {
	Cookie string
}

func SGSave(data TJSG) {
	marshal, err := json.Marshal(data)
	if err != nil {
		return
	}
	insert(KTJSG, string(marshal))
}

func SGGet() (s TJSG) {
	s2 := read(KTJSG)
	json.Unmarshal([]byte(s2), &s)
	return
}
