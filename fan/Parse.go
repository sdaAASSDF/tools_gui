package fan

import (
	"regexp"
	"strconv"
	"strings"
)

func Parse(_url string) string {
	findString := regexp.MustCompile(`\{.*?\}`).ReplaceAllStringFunc(_url, func(s string) string {
		s = strings.Trim(s, "}")
		s = strings.Trim(s, "{")
		rule := strings.Join(regexp.MustCompile(`\p{Han}`).FindAllString(s, -1), "")
		//提取大小
		_size := regexp.MustCompile(`\d`).FindAllString(s, -1)
		size := []int{}
		for _, v := range _size {
			a, _ := strconv.Atoi(v)
			if a == 0 {
				continue
			}
			size = append(size, a)
		}
		if len(size) < 1 && len(size) > 2 {
			return ""
		}
		switch rule {
		case "数字":
			return Number(size)
		case "字母":
			return strings.ToLower(ZiMu(size))
		case "小写字母":
			return strings.ToLower(ZiMu(size))
		case "大写字母":
			return strings.ToUpper(ZiMu(size))
		case "随机":
			return Random(size)
		}
		return ""
	})
	return findString
}
