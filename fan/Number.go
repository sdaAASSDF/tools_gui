package fan

import (
	"github.com/123456/c_code"
	"math/rand"
)

func Number(size []int) string {
	zz := `0123456789`
	ctreate := size[0]
	if len(size) == 2 {
		ctreate = c_code.RandDom(size[0], size[1])
	}
	b := make([]byte, ctreate)
	for i := range b {
		b[i] = zz[rand.Intn(len(zz))]
	}
	return string(b)
}

//随机字母
func ZiMu(size []int) string {
	zz := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	ctreate := size[0]
	if len(size) == 2 {
		ctreate = c_code.RandDom(size[0], size[1])
	}
	b := make([]byte, ctreate)
	for i := range b {
		b[i] = zz[rand.Intn(len(zz))]
	}
	return string(b)
}

//随机字符
func Random(size []int) string {
	zz := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	ctreate := size[0]
	if len(size) == 2 {
		ctreate = c_code.RandDom(size[0], size[1])
	}
	b := make([]byte, ctreate)
	for i := range b {
		b[i] = zz[rand.Intn(len(zz))]
	}
	return string(b)
}
