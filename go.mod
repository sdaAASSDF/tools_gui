module seo_tools

go 1.14

require (
	github.com/123456/c_code v0.0.0-00010101000000-000000000000
	github.com/GeertJohan/go.rice v1.0.0
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394
	github.com/boltdb/bolt v1.3.1
	github.com/chromedp/cdproto v0.0.0-20200709115526-d1f6fc58448b
	github.com/chromedp/chromedp v0.5.3
	github.com/daaku/go.zipexe v1.0.1 // indirect
	github.com/disintegration/imaging v1.6.2
	github.com/fengmangseo/fm v0.0.0-20200204094737-f34a33b5a71c
	github.com/foolin/goview v0.3.0
	github.com/gin-gonic/gin v1.6.3
	github.com/go-resty/resty/v2 v2.3.0
	github.com/tidwall/gjson v1.6.0
	github.com/wumansgy/goEncrypt v0.0.0-20190822060801-cf9a6f8787e4
	github.com/zserge/lorca v0.1.9
	go.zoe.im/surferua v0.0.1
)

replace github.com/123456/c_code => E:\codeing\go\c_code
