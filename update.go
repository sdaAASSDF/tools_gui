package main

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
)

func main() {
	os.Mkdir("_exe", os.ModePerm)
	down_list := []map[string]string{
		{
			"u": "gui.vmp.exe",
			"s": "gui.exe",
		},
		{
			"u": "chromedriver.exe",
			"s": "_exe/chromedriver.exe",
		},
		{
			"u": "help1.exe",
			"s": "_exe/help1.exe",
		},
	}
	input := ""
	for _, v := range down_list {
		err := downloadFile("http://feitailang233.gitee.io/acc/"+v["u"], v["s"], func(file_name string, length, downLen int64) {
			fmt.Printf("%s 下载进度 %d  %s \r", file_name, int(float32(downLen)/float32(length)*100), "%")

		})
		fmt.Println("\r\n\r\n  ---- >")
		if err != nil {
			fmt.Println(v["s"], "下载失败")
			fmt.Println("错误信息 ->", err.Error())
			fmt.Println("输入任意键结束")
			fmt.Scanln(&input)
			break
		}
	}
	fmt.Println("更新完成,输入任意键结束")
	fmt.Scanln(&input)
}

func downloadFile(url, file_name string, fb func(file_name string, length, downLen int64)) error {
	var (
		fsize   int64
		buf     = make([]byte, 32*1024)
		written int64
	)
	//创建一个http client
	client := new(http.Client)
	//get方法获取资源
	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	//读取服务器返回的文件大小
	fsize, err = strconv.ParseInt(resp.Header.Get("Content-Length"), 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	//创建文件
	file, err := os.Create(file_name)
	if err != nil {
		return err
	}
	defer file.Close()
	if resp.Body == nil {
		return errors.New("body is null")
	}
	defer resp.Body.Close()
	//下面是 io.copyBuffer() 的简化版本
	for {
		//读取bytes
		nr, er := resp.Body.Read(buf)
		if nr > 0 {
			//写入bytes
			nw, ew := file.Write(buf[0:nr])
			//数据长度大于0
			if nw > 0 {
				written += int64(nw)
			}
			//写入出错
			if ew != nil {
				err = ew
				break
			}
			//读取是数据长度不等于写入的数据长度
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}
		if er != nil {
			if er != io.EOF {
				err = er
			}
			break
		}
		//没有错误了快使用 callback

		fb(file_name, fsize, written)
	}
	return err
}
