package api

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"os"
	"seo_tools/db"
	"seo_tools/verifyauth"
)

func res() db.APIRESULT {
	return db.APIRESULT{
		Data:    gin.H{},
		Time:    verifyauth.TimeSign(),
		Version: verifyauth.Version(),
		Err:     "",
	}

}
func render(c *gin.Context, d db.APIRESULT) {
	//输出
	data, _ := verifyauth.EncryptData(d)

	c.String(200, data)
	c.Abort()
}

func FKSS() gin.HandlerFunc {
	return func(c *gin.Context) {
		fkss := c.GetHeader("fkss")
		if fkss == "" {
			c.String(200, "100")
			c.Abort()
			return
		}
		//进行解密
		sqm, isok := verifyauth.DecryptData(fkss)
		if !isok {
			apiresult := res()
			apiresult.Err = "授权码无效111"

			render(c, apiresult)
			return
		}
		//验证时间是否正确
		if !verifyauth.TimeVerify(sqm.Time) {
			apiresult := res()
			apiresult.Err = "授权码无效222"
			render(c, apiresult)
			return
		}
		//验证版本号
		if sqm.Version != verifyauth.Version() {
			apiresult := res()
			apiresult.Err = "找客服更新到最新版本"
			render(c, apiresult)
			return
		}
		//验证授权码正确性
		if sqm.Code == "" {
			apiresult := res()
			apiresult.Err = "未填写授权码"
			render(c, apiresult)
			return
		}
		code_file := "code/" + sqm.Code
		file, err := ioutil.ReadFile(code_file)
		if err != nil {
			apiresult := res()
			apiresult.Err = "授权码无效333"
			render(c, apiresult)
			return
		}

		//if !c_code.IsFile(code_file) {
		//	apiresult := res()
		//	apiresult.Err = "授权码无效333"
		//	render(c, apiresult)
		//}
		//检测UUID 是否 通过
		v_f := db.SQMFILE{}
		//file, err := ioutil.ReadFile(code_file)
		//未绑定直接绑定
		if string(file) == "" {
			v_f.CPUID = sqm.CPUID
			v_f.UUID = sqm.UUID
			marshal, _ := json.Marshal(v_f)
			ioutil.WriteFile(code_file, marshal, os.ModePerm)
			c.Next()
			return
		}

		json.Unmarshal(file, &v_f)

		if v_f.UUID != sqm.UUID || v_f.CPUID != sqm.CPUID {
			apiresult := res()
			apiresult.Err = "授权码无效444"
			render(c, apiresult)
			return
		}
	}
}
