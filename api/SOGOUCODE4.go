package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httputil"
	"net/url"
)

func SOGOUCODE4(c *gin.Context) {
	porxy_server(c.Writer, c.Request)
}

func porxy_server(w http.ResponseWriter, r *http.Request) {
	//remote,_:= url.Parse("http://139.155.58.169:5000" )

	//
	//proxy := httputil.NewSingleHostReverseProxy(remote)
	//proxy.ServeHTTP(w, r)
	//r.Host = "you.host.here:port"
	u, _ := url.Parse("http://127.0.0.1:5000")
	r.URL.Path = "/b"
	proxy := httputil.NewSingleHostReverseProxy(u)
	// You can optionally capture/wrap the transport if that's necessary (for
	// instance, if the transport has been replaced by middleware). Example:
	// proxy.Transport = &myTransport{proxy.Transport}
	proxy.Transport = &myTransport{}

	proxy.ServeHTTP(w, r)
}

type myTransport struct {
	// Uncomment this if you want to capture the transport
	// CapturedTransport http.RoundTripper
}

func (t *myTransport) RoundTrip(request *http.Request) (*http.Response, error) {
	response, err := http.DefaultTransport.RoundTrip(request)
	// or, if you captured the transport
	// response, err := t.CapturedTransport.RoundTrip(request)

	// The httputil package provides a DumpResponse() func that will copy the
	// contents of the body into a []byte and return it. It also wraps it in an
	// ioutil.NopCloser and sets up the response to be passed on to the client.
	//body, err := httputil.DumpResponse(response, true)
	//if err != nil {
	//	// copying the response body did not work
	//	return nil, err
	//}

	// You may want to check the Content-Type header to decide how to deal with
	// the body. In this case, we're assuming it's text.
	//str, err := ioutil.ReadAll(response.Body)
	//if err != nil {
	//	return nil,err
	//}
	//apiresult := res()
	//
	////regexp.MustCompile(``)
	//apiresult.Data["value"] =
	//data, _ := verifyauth.EncryptData(apiresult)
	//value := gjson.Get(string(body), "value").String()
	//response.ContentLength = int64(len(value))
	//response.Body = ioutil.NopCloser(strings.NewReader(value))
	return response, err
}
