package gui

import (
	"github.com/go-resty/resty/v2"
	"github.com/tidwall/gjson"

	"seo_tools/verifyauth"
)

func (g *G) VPOST_SOGOU_4() (txt string, err error) {

	code, err_str := check()
	if err_str != "" {
		g.ShowError(err_str)
		return
	}

	_url := verifyauth.API("/sogou_code/4")
	client := resty.New()
	_chile_headr := map[string]string{}
	_chile_headr["UserAgent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"
	_chile_headr["Fkss"] = code
	_result, err := client.R().
		SetHeaders(_chile_headr).SetFile("image_file", CaptchaName).Post(_url)
	if err != nil {
		return
	}
	content := ""
	content = _result.String()
	txt = gjson.Get(content, "value").String()
	return
}
