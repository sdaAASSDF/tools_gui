package sogou

import (
	"encoding/json"
	"fmt"
	"github.com/123456/c_code"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
	"github.com/tidwall/gjson"
	"seo_tools/db"
	"seo_tools/kc"
	"strings"
	"time"
)

//http://zhanzhang.sogou.com/index.php/sitelink/index
//搜狗提交  1

type submit_1_s struct {
	Type     string `json:"type"`
	Email    string `json:"email"`
	Min      int    `json:"min"`
	Max      int    `json:"max"`
	Textarea string `json:"textarea"`
}

func (g_ *sogou) submit_1(_str string) {
	_f := submit_1_s{}
	err := json.Unmarshal([]byte(_str), &_f)
	fmt.Println(err)
	data := _f.Textarea
	_type := _f.Type
	email := _f.Email
	g_.MessageClear()
	list := []string{}
	for _, v := range strings.Split(strings.ReplaceAll(data, "\r", ""), "\n") {
		v = strings.TrimSpace(v)
		if v == "" {
			continue
		}
		list = append(list, v)
	}
	if len(list) < 1 {
		g_.MessageError("链接数少于 1 ")
		return
	}
	chunk := c_code.ArrayChunkString(list, 20)
	message := fmt.Sprintf("共 %d 条链接 需提交 %d 次 当前 %d ", len(list), len(chunk), 0)
	g_.Message(message)
	for k, v := range chunk {
		g_.submit_1_push(v, _type, email)
		message = fmt.Sprintf("共 %d 条链接 需提交 %d 次 当前 %d ", len(list), len(chunk), k+1)
		g_.Message(message)
		if _f.Min != 0 && _f.Max != 0 {
			t := c_code.RandDom(_f.Min, _f.Max)
			g_.Message(fmt.Sprintf("暂停 %d 秒", t))
			time.Sleep(time.Second * time.Duration(t))
		}
	}
	g_.Message("提交完成")
}
func (g_ *sogou) submit_1_push(link []string, site_type string, email string) (isok bool) {

	c := &kc.KcBody{
		UA:      "",
		TempDir: "c:\\suoha\\seo\\sogou",
	}
	c.Up()
	defer c.Close()
	g_.Message("进入提交页面")
	//chromedp.Run(c.Ctx,chromedp.Se("http://zhanzhang.sogou.com/index.php/sitelink/index"))
	if site_type != "2" {
		site_type = "1"
	}
	if email == "" {
		email = c_code.RandEmail(c_code.RandDom(5, 13))
	}
	chanajaxstr := make(chan string, 1)
	submit_1_captcha(c.Ctx, chanajaxstr)
	chromedp.Run(c.Ctx,
		network.Enable(),
		c.SetCookieNavigate("http://zhanzhang.sogou.com/index.php/sitelink/index", db.SGGet().Cookie),
	)

	chromedp.Run(c.Ctx,
		chromedp.SendKeys("#webAdr", strings.Join(link, "\n"), chromedp.ByID),
		chromedp.Click(fmt.Sprintf(`input[name="site_type"][value="%s"]`, site_type), chromedp.ByQuery),
		chromedp.SetValue(`input[name="email"]`, email, chromedp.ByQuery),
	)

	stop := 4
	for {
		if stop < 1 {
			break
		}
		//调用验证码识别
		sogou_4, err := g_.VPOST_SOGOU_4()
		if err != nil && len(sogou_4) != 4 {
			g_.MessageError("验证码识别失败")
			stop--
			continue
		}
		//sogou_4 = "124235"
		//输入验证码
		chromedp.Run(c.Ctx,
			network.Enable(),
			chromedp.SetValue("#jsVcode", sogou_4, chromedp.ByID),
			chromedp.Click(`a.btn_add[onClick]`, chromedp.ByQuery),
		)
		//获取验证码返回值

		g_.Message(fmt.Sprintf("验证码识别为 %s ", sogou_4))
		g_.DaMa()
		result := <-chanajaxstr
		if !gjson.Get(result, "success").Bool() {
			g_.MessageError("验证码错误")
			time.Sleep(time.Second)
			continue
		}
		result = <-chanajaxstr
		if !gjson.Get(result, "success").Bool() {
			g_.MessageError("提交失败")
			break
		}
		g_.Message("提交成功")
		time.Sleep(time.Second * 2)
		break
	}

	return
}
