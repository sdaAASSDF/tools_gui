package sogou

import (
	"context"
	"fmt"
	"github.com/123456/c_code"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"seo_tools/db"
	"seo_tools/gui"
	"seo_tools/h"
	"seo_tools/kc"
	"strings"
	"time"
)

func submit_1_captcha(ctx context.Context, logninok chan (string)) {
	chromedp.ListenTarget(ctx, func(ev interface{}) {
		switch ev := ev.(type) {
		case *network.EventResponseReceived:
			resp := ev.Response
			if len(resp.Headers) != 0 {
				r_url, e := url.QueryUnescape(resp.URL)
				if e != nil {
					return
				}
				if _, ok := resp.Headers["Content-Type"]; ok {
					fmt.Println(resp.Headers["Content-Type"])
					//下载图片
					p_url, e := url.Parse(r_url)
					content_type := strings.ToLower(resp.Headers["Content-Type"].(string))
					if strings.Contains(content_type, "image") {

						if !strings.HasSuffix(p_url.Path, "vcodefb") {
							return
						}

						fmt.Println(p_url, e, resp.Headers["Content-Type"])
						go func() {
							if err := chromedp.Run(ctx, chromedp.ActionFunc(func(ctx context.Context) error {
								body, err := network.GetResponseBody(ev.RequestID).Do(ctx)
								if err != nil {

									return err
								}
								err = ioutil.WriteFile(gui.CaptchaName, body, os.ModePerm)

								//respBody <- string(body)
								//return err
								return err
							})); err != nil {
								return
							}
						}()
					}

					//监听AJAX请求登录请求
					if strings.Contains(content_type, "text/html") && (strings.HasSuffix(p_url.Path, "vcodefb") || strings.HasSuffix(p_url.Path, "urlSubmit/create")) {
						go func() {
							if err := chromedp.Run(ctx, chromedp.ActionFunc(func(ctx context.Context) error {
								body, err := network.GetResponseBody(ev.RequestID).Do(ctx)
								if err != nil {

									return err
								}

								logninok <- string(body)

								return err
							})); err != nil {
								return
							}
						}()
					}
				}
			}

		}
	})
}

func login_captcha(ctx context.Context) {
	chromedp.ListenTarget(ctx, func(ev interface{}) {
		switch ev := ev.(type) {
		case *network.EventResponseReceived:
			resp := ev.Response
			if len(resp.Headers) != 0 {
				r_url, e := url.QueryUnescape(resp.URL)
				if e != nil {
					return
				}
				if _, ok := resp.Headers["Content-Type"]; ok {
					fmt.Println(resp.Headers["Content-Type"])
					//下载图片
					p_url, e := url.Parse(r_url)
					content_type := strings.ToLower(resp.Headers["Content-Type"].(string))
					if strings.Contains(content_type, "image") {

						if !strings.HasSuffix(p_url.Path, "vcode") {
							return
						}

						fmt.Println(p_url, e, resp.Headers["Content-Type"])
						go func() {
							if err := chromedp.Run(ctx, chromedp.ActionFunc(func(ctx context.Context) error {
								body, err := network.GetResponseBody(ev.RequestID).Do(ctx)
								if err != nil {

									return err
								}
								err = ioutil.WriteFile(gui.CaptchaName, body, os.ModePerm)

								//respBody <- string(body)
								//return err
								return err
							})); err != nil {
								return
							}
						}()
					}
					////监听AJAX请求登录请求
					//if strings.Contains(content_type, "text/html") && strings.HasSuffix(p_url.Path, "login") {
					//	go func() {
					//		if err := chromedp.Run(ctx, chromedp.ActionFunc(func(ctx context.Context) error {
					//			body, err := network.GetResponseBody(ev.RequestID).Do(ctx)
					//			if err != nil {
					//
					//				return err
					//			}
					//
					//			logninok <- string(body)
					//
					//			return err
					//		})); err != nil {
					//			return
					//		}
					//	}()
					//}
				}
			}

		}
	})
}

//搜狗登录完成
func (g_ *sogou) login(user, pwd string) {
	g_.MessageClear()
	if user == "" {
		g_.MessageError("账号必填")
		return
	}
	if pwd == "" {
		g_.MessageError("密码必填")
		return
	}
	g_.Message("打开浏览器")

	c := kc.KcBody{
		UA:      "",
		TempDir: "c:\\suoha\\seo\\sogou",
	}
	c.Up()
	defer c.Close()
	g_.Message("进入登录页面")
	err := chromedp.Run(c.Ctx, chromedp.Navigate("http://zhanzhang.sogou.com/"))
	if err != nil {
		g_.MessageError("进入页面失败")
		return
	}
	g_.Message("开始登录")
	//迭代等待直到点击成功
	stop := 5
	for {
		err = chromedp.Run(c.CtxTimeOutCtx(time.Second*3),
			chromedp.Click("#loginLink", chromedp.ByID),
		)
		if err == nil {
			break
		}
		g_.MessageError("寻找节点超时")

		stop--
		if stop < 1 {
			break
		}
	}
	if stop < 1 {
		g_.MessageError("浏览器响应超时")
		return
	}
	g_.Message("开始输入账号")
	err = chromedp.Run(c.CtxTimeOutCtx(time.Second*3),
		chromedp.SendKeys("#js_username", user, chromedp.ByID),
		chromedp.SendKeys("#js_password", pwd, chromedp.ByID),
	)
	if err != nil {
		g_.MessageError("输入账号密码出错了")
		return
	}

	login_captcha(c.Ctx)

	stop = 7

	for {
		if stop < 1 {
			break
		}
		//切换验证码
		var clear string
		chromedp.Run(c.CtxTimeOutCtx(time.Second*3),
			network.Enable(),
			chromedp.Click("#js_vcode_img", chromedp.ByID),
			chromedp.Evaluate("document.querySelector('#js_vcode_input').value=''", &clear),
		)
		time.Sleep(time.Millisecond * 300)
		//检测验证码文件是否存在
		if !c_code.IsFile(gui.CaptchaName) {
			stop--
			g_.MessageError("验证码文件不存在")
			continue
		}

		time.Sleep(time.Second)
		g_.DaMa()
		sogou_4, err := g_.VPOST_SOGOU_4()
		if err != nil && len(sogou_4) != 4 {
			g_.MessageError("验证码识别失败")
			stop--
			continue
		}

		err = chromedp.Run(c.CtxTimeOutCtx(time.Second*3),
			network.Enable(),
			chromedp.SendKeys("#js_vcode_input", sogou_4, chromedp.ByID),
			chromedp.Sleep(time.Second),
			chromedp.Click("#js_submit_login_btn", chromedp.ByID))
		if err != nil {
			g_.Message("句柄丢失")
			return
		}
		time.Sleep(time.Second * 1)
		cookie := c.GetCookieStr()
		content, err := h.Get("http://zhanzhang.sogou.com/", map[string]string{}, cookie)
		if err != nil {
			stop--
			continue
		}
		log.Println(cookie)
		user_name := getusername(content)
		if user_name == "" {
			g_.MessageError("登录失败再次尝试登录")
			stop--
			continue
		}
		db.SGSave(db.TJSG{Cookie: cookie})
		g_.Message("登录成功")
		c.Close()
		g_.Reload()

		break
	}
	if stop < 1 {
		g_.MessageError("尝试多次登录失败，请检测密码是否错误")
		return
	}
}
func getusername(context string) string {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(context))
	if err != nil {
		return ""
	}
	doc.Clone()
	return strings.TrimSpace(doc.Find("#userArea .user_name").Text())
}
