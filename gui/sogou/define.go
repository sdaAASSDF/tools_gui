package sogou

import (
	"seo_tools/gui"
)

type sogou struct {
	*gui.G
}

//注册Sogou相关的
var _sogou *sogou

func Register(g *gui.G) {
	_sogou = &sogou{g}
	_sogou.UI.Bind("sg_login", _sogou.login)
	_sogou.UI.Bind("sg_submit", _sogou.submit_1)
}
