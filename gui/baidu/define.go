package baidu

import (
	"seo_tools/gui"
)

type baidu struct {
	*gui.G
}

//注册Sogou相关的
var _baidu *baidu

func Register(g *gui.G) {
	_baidu = &baidu{g}
	// 百度PC排名查询
	_baidu.UI.Bind("baidu_pm_pc", _baidu.pm_pc)
	_baidu.UI.Bind("baidu_pm_m", _baidu.pm_m)
}
