package baidu

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
	"seo_tools/kc"
	"strings"
	"time"
)

func (g_ *baidu) pm_m(_str string) {
	_f := pm_pc_s{}
	json.Unmarshal([]byte(_str), &_f)
	c := &kc.KcBody{}
	c.Up()
	defer c.Off()
	chromedp.Run(c.Ctx, chromedp.Navigate("https://m.baidu.com"))
	chromedp.Run(c.Ctx,
		chromedp.SendKeys("#index-kw", _f.CI, chromedp.ByQuery),
		chromedp.Click("#index-bn", chromedp.ByQuery),
	)
	time.Sleep(time.Second)
	max_page := _f.Stop
	if max_page == 0 {
		max_page = 10
	}
	g_.MessageClear()
	g_.UI.Eval("app.setf1pm('')")
	g_.Message("开始查询")

	start_page := 1
	//提取页面
	pm, next, text_list := pm_m_ok(c, _f.Domain, _f.Txt)
	for ttt_index, ttt := range text_list {
		pm_show := ttt_index + 1
		g_.Message(fmt.Sprintf("%d --> %s", pm_show, ttt))
	}
	if pm == 0 && next {
		for {
			//点击下一页
			chromedp.Run(c.Ctx,
				chromedp.Click(`#page-controller a[class*="new-nextpage"]`, chromedp.ByQuery),
				chromedp.Sleep(time.Millisecond*300),
				chromedp.WaitReady("body"),
			)
			pm, next, text_list = pm_m_ok(c, _f.Domain, _f.Txt)
			for ttt_index, ttt := range text_list {
				pm_show := start_page*10 + ttt_index + 1
				g_.Message(fmt.Sprintf("%d --> %s", pm_show, ttt))
			}

			if pm != 0 {
				pm = start_page*10 + pm
				break
			}
			if !next {
				break
			}
			if start_page >= max_page {
				break
			}
			start_page++
		}
	}
	if !next {
		g_.MessageError("没有下一页,执行退出操作")
	}
	if pm == 0 {
		g_.MessageError("未找到排名")
	} else {
		g_.Message("你的排名为 " + fmt.Sprintf("%d", pm))
		g_.UI.Eval("app.setf1pm(" + fmt.Sprintf("%d", pm) + ")")
	}

}

func pm_m_ok(c *kc.KcBody, domain, txt string) (pm int, next bool, l []string) {
	time.Sleep(time.Second)
	pm_m_click_mseeage(c)
	html := c.Html(c.Ctx)
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if err != nil {
		return
	}
	defer doc.Clone()
	if doc.Find("div.c-result-content").Size() == 0 {
		return
	}
	is_ok := false
	doc.Find("div.c-result-content").Each(func(i int, selection *goquery.Selection) {
		selection.Find("style").Remove()
		text := selection.Find(".c-showurl.c-footer-showurl").Text()
		if text == "" {
			//outerHtml, _ := goquery.OuterHtml(selection)
			//fmt.Println(outerHtml)
			text = selection.Find(".c-color-gray").Last().Text()
			if text == "百度APP内打开" {
				return
			}
		}
		l = append(l, text)
		if strings.Contains(text, domain) && !is_ok && domain != "" {
			pm = i + 1
			is_ok = true
			return
		}
		if strings.Contains(text, txt) && !is_ok && txt != "" {
			pm = i + 1
			is_ok = true
			return
		}
	})
	//检测是否存在下一页
	next_text := doc.Find(`#page-controller a[class*="new-nextpage"]`).Size()
	if next_text == 1 {
		next = true
	}
	return
}

func pm_m_click_mseeage(c *kc.KcBody) {
	html := c.Html(c.Ctx)
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if err != nil {
		return
	}
	defer doc.Clone()
	if doc.Find(".ivk-button.ivk-button-popup").Size() == 0 {
		return
	}
	//点击展开更多搜索结果
	chromedp.Run(c.Ctx, chromedp.Click(".ivk-button.ivk-button-popup", chromedp.ByQuery))
	time.Sleep(time.Millisecond * 300)
	//点击取消
	chromedp.Run(c.Ctx, chromedp.Click("#popupLead .popup-lead-cancel", chromedp.ByQuery))
	time.Sleep(time.Millisecond * 300)

}
