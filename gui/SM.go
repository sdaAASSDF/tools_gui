package gui

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"net/http"
	"regexp"
	"seo_tools/db"
	"seo_tools/h"
	"seo_tools/kc"
	"strings"
	"time"
)

func (g *G) TJSMGetCookie() {
	c := kc.KcBody{
		UA:      "",
		TempDir: "c:\\suoha\\seo\\sm",
	}
	c.Up()
	defer c.Close()
	err := chromedp.Run(c.Ctx, chromedp.Navigate("https://zhanzhang.sm.cn/index.php"))
	if err != nil {
		return
	}
	//监听是否已登录 , 已登录获取cookie 直接关闭
	loca_url := ""
	for {
		err := chromedp.Run(c.Ctx, chromedp.Location(&loca_url))
		if err != nil {
			break
		}
		time.Sleep(time.Millisecond * 300)
		println(loca_url)
		if loca_url == "https://zhanzhang.sm.cn/open/detialPage" {
			break
		}
	}
	//提取Cookie 保存到本地
	cookie := ""
	chromedp.Run(c.Ctx, chromedp.Evaluate(`document.cookie`, &cookie))
	if cookie == "" {
		return
	}
	//验证COOKIE 是否可用
	content, err := h.Get("https://zhanzhang.sm.cn/open/detialPage?order=1&view=1", map[string]string{}, cookie)
	if err != nil {
		return
	}
	reader, err := goquery.NewDocumentFromReader(strings.NewReader(content))
	if err != nil {
		return
	}
	defer reader.Clone()
	text := reader.Find(".name").Text()
	if text == "" {
		return
	}

	tjsm := db.TJSM{Cookie: cookie}
	db.SMSave(tjsm)
	//刷新页面
	g.Reload()
}

func (g *G) TJSMSetCookie(cookie string) {
	sm := db.SMGet()
	sm.Cookie = cookie
	db.SMSave(sm)
	g.Reload()
}
func (g *G) TJSMChangeDomain(href string) {
	sm := db.SMGet()
	get, err := h.Get("https://zhanzhang.sm.cn"+href, map[string]string{}, sm.Cookie)
	if err != nil {
		return
	}

	// https://data.zhanzhang.sm.cn/push?site=studyseo.net&user_name=57159300@qq.com&resource_name=mip_add&token=TI_2d9e2201595398070732d121a4c7c4f5
	findString := regexp.MustCompile(`http.*?token=[\s|\w]{8,60}`).FindString(get)
	g.UI.Eval("$('#s_link').val(\"" + findString + "\")")
}
func (g *G) TJSMSubmit(href string, data string) {
	list := []string{}
	for _, v := range strings.Split(strings.ReplaceAll(data, "\r", ""), "\n") {
		v = strings.TrimSpace(v)
		if v == "" {
			continue
		}
		list = append(list, v)
	}
	if len(list) < 1 {
		return
	}
	//验证完成开始推送
	post := ___sm_post(href, list)

	post = regexp.MustCompile(`[\r|\r\n|\n]`).ReplaceAllString(post, "")

	code := gjson.Get(post, "returnCode").Int()
	message := ""
	switch code {
	case 200:
		message = "提交成功"
		break
	case 201:
		message = "token不合法"
		break
	case 202:
		message = "当日流量已用完"
		break
	case 400:
		message = "请求参数有误"
		break
	case 500:
		message = "服务器内部错误"
		break
	}
	//201: token不合法; 202: 当日流量已用完; 400: 请求参数有误; 500: 服务器内部错误
	g.Message(fmt.Sprintf(message + " -- " + post))
	println(post)
	return
}
func (g *G) TJSMCancel(href string, data string) {
	href = strings.ReplaceAll(href, "resource_name=mip_add", "resource_name=mip_clean")

	list := []string{}
	for _, v := range strings.Split(strings.ReplaceAll(data, "\r", ""), "\n") {
		v = strings.TrimSpace(v)
		if v == "" {
			continue
		}
		list = append(list, v)
	}
	if len(list) < 1 {
		return
	}
	//验证完成开始推送
	post := ___sm_post(href, list)

	post = regexp.MustCompile(`[\r|\r\n|\n]`).ReplaceAllString(post, "")

	code := gjson.Get(post, "returnCode").Int()
	message := ""
	switch code {
	case 200:
		message = "提交成功"
		break
	case 201:
		message = "token不合法"
		break
	case 202:
		message = "当日流量已用完"
		break
	case 400:
		message = "请求参数有误"
		break
	case 500:
		message = "服务器内部错误"
		break
	}
	//201: token不合法; 202: 当日流量已用完; 400: 请求参数有误; 500: 服务器内部错误
	g.Message(fmt.Sprintf(message + " -- " + post))
	println(post)
	return
}

func ___sm_post(api string, urls []string) (res_data string) {
	client := &http.Client{}
	post_data := strings.Join(urls, "\n")
	req, err := http.NewRequest("POST", api, strings.NewReader(post_data))
	if err != nil {
		// handle error
		return
	}

	req.Header.Set("Content-Type", "text/plain")

	resp, err := client.Do(req)
	if err != nil {
		return
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
		return
	}

	res_data = string(body)
	return
}
