package gui

import (
	"fmt"

	"log"
	"net/http"
	"strings"
)

type Self struct {
	SetCookie   string
	ReqCookie   string
	Cookies     []*http.Cookie
	Message     string
	ResultStr   string
	Client      *http.Client
	ClickTxt    []string
	SubmitToken string
	Ua          string
}

func (se *Self) GetCookieStr(cookie_list []string, cookie_jar []*http.Cookie) {

	cookie := []string{}
	if len(cookie_list) != 0 {
		for _, v := range cookie_list {
			cookie_list := strings.Split(v, ";")
			if len(cookie_list) == 0 {
				continue
			}
			cookie_list = strings.Split(cookie_list[0], "=")
			if len(cookie_list) == 2 {
				cookie = append(cookie, fmt.Sprintf("%s=%s", strings.TrimSpace(cookie_list[0]), strings.TrimSpace(cookie_list[1])))
			}

		}
	}
	if len(cookie_jar) != 0 {
		for _, line := range cookie_jar {
			cookie = append(cookie, fmt.Sprintf("%s=%s", line.Name, line.Value))
		}
	}
	se.ReqCookie = strings.Join(cookie, "; ")
	println(se.ReqCookie)
}

func (se *Self) Dump(txt string) {
	log.Println(txt)
	se.Message = txt
}
func (se *Self) ClearSetup() {
	se.Message = ""
	se.ResultStr = ""
}
