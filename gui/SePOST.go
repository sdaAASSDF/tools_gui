package gui

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

func (se *Self) POST(_url string, post_data url.Values) (isok bool) {

	//进入Sogou页面
	req, err := http.NewRequest("POST", _url, strings.NewReader(post_data.Encode()))
	if err != nil {
		//创建err请求头失败
		se.Dump("创建POST请求头失败")
		return
	}
	////如果存在Cookie 则 添加
	//for _, cookie := range se.Cookies {
	//	req.AddCookie(cookie)
	//}
	//设置 Cookie
	if se.SetCookie != "" {
		req.Header.Set("Cookie", se.SetCookie)
	}
	//设置UA 头
	req.Header.Set("User-Agent", se.Ua)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	do, err := se.Client.Do(req)
	if err != nil {
		se.Dump("接口未正确响应")
		return
	}
	r_cookies := req.Cookies()

	se.GetCookieStr(do.Header.Values("Set-Cookie"), r_cookies)
	//得到返回值
	defer do.Body.Close()

	_bi, err := ioutil.ReadAll(do.Body)
	if err != nil {
		se.Dump("读取返回值失败")
		return
	}
	se.ResultStr = string(_bi)

	isok = true

	return
}
