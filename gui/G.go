package gui

import (
	"crypto/tls"
	"fmt"
	"github.com/123456/c_code"
	"github.com/zserge/lorca"
	"go.zoe.im/surferua"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"seo_tools/db"
	"time"
)

//
func CreateSelf() *Self {
	//使用https，设置不验证
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	//jar, _ := cookiejar.New(nil)
	http_client := &http.Client{
		Transport: tr,
		//Jar:       jar,
	}

	http_client.Timeout = 1 * time.Minute
	se := &Self{
		Client: http_client,
		Ua:     surferua.New().Windows().String(),
	}

	return se
}

type G struct {
	UI lorca.UI
}

func (g *G) Html(sel string, val string) {
	runjs := "document.querySelector('" + sel + "').innerHTML=`" + val + "`"
	fmt.Println(runjs)
	g.UI.Eval(runjs)
}
func (g *G) MessageClear() {
	runjs := fmt.Sprintf("document.querySelector('#runtime-message-show').innerHTML=\"\"")
	runjs = fmt.Sprintf("$('#runtime-message-show').html('')")
	fmt.Println(runjs)
	g.UI.Eval(runjs)
	g.UI.Eval(`document.querySelector('#runtime-message').scrollTo(0,document.querySelector('#runtime-message').scrollHeight)`)
}

func (g *G) DaMa() {
	runjs := fmt.Sprintf("$('#runtime-message-show').append('<div style=\"color:blue\">C： 开始打码</div><div style=\"color:red\">< 技术可出售，联系技术，价格美丽 ></div>')")
	g.UI.Eval(runjs)
	g.UI.Eval(`document.querySelector('#runtime-message').scrollTo(0,document.querySelector('#runtime-message').scrollHeight)`)
}
func (g *G) Message(txt string) {
	runjs := fmt.Sprintf("document.querySelector('#runtime-message-show').insertAdjacentHTML('afterend','<div>%s</div>')", txt)
	runjs = fmt.Sprintf("$('#runtime-message-show').append('<div>%s</div>')", txt)
	fmt.Println(runjs)
	g.UI.Eval(runjs)
	g.UI.Eval(`document.querySelector('#runtime-message').scrollTo(0,document.querySelector('#runtime-message').scrollHeight)`)
}
func (g *G) MessageError(txt string) {
	runjs := fmt.Sprintf("document.querySelector('#runtime-message-show').insertAdjacentHTML('afterend','<div>%s</div>')", txt)
	runjs = fmt.Sprintf("$('#runtime-message-show').append('<div style=\"color:red\">X： %s</div>')", txt)
	fmt.Println(runjs)
	g.UI.Eval(runjs)
	g.UI.Eval(`document.querySelector('#runtime-message').scrollTo(0,document.querySelector('#runtime-message').scrollHeight)`)
}

//刷新页面
func (g *G) Reload() {
	g.UI.Eval(`location.reload()`)
}

//进入其他页面
func (g *G) Location(_url string) {
	runjs := fmt.Sprintf("window.location.href=\"%s\"", _url)
	fmt.Println(runjs)
	g.UI.Eval(runjs)
}

//跳转到错误页面
func (g *G) ShowError(s string) {
	runjs := fmt.Sprintf("window.location.href=\"/error?message=%s\"", s)
	db.SQMSET(false)
	fmt.Println(runjs)
	g.UI.Eval(runjs)
}

//保存验证码
func (g *G) CaptchaISOK(ibase string) (isok bool) {
	re := `data:i.*?/.{3,7};base64,`
	if !regexp.MustCompile(re).MatchString(ibase) {
		log.Println("不是base格式")
		return
	}
	ibase = regexp.MustCompile(re).ReplaceAllString(ibase, "")
	//decodeString, err := base64.RawStdEncoding.DecodeString(ibase)
	decodeString, err := c_code.Bs64Decode(ibase)
	if err != nil {
		log.Println("格式转换失败")
		return
	}
	err = ioutil.WriteFile(CaptchaName, []byte(decodeString), os.ModePerm)
	if err != nil {
		log.Println("写入文件失败")
		return
	}
	img, err := c_code.IsImg(CaptchaName)
	if err != nil {
		return
	}
	if !img {
		return
	}
	//检测图片是否存在
	return true
}
