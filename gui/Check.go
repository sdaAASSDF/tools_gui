package gui

import (
	"github.com/123456/c_code"
	"os/exec"
	"seo_tools/db"
	"seo_tools/verifyauth"
	"strings"
)

var _uuid = ""
var _cpu_id = ""

func check() (code string, err_str string) {

	d := db.SQM{
		Time:    verifyauth.TimeSign(),
		Code:    db.AUTHGET(),
		Version: verifyauth.Version(),
		CPUID:   "",
		UUID:    "",
		Rand:    c_code.RandDom(1, 10000),
	}

	//cpu ID

	if _uuid == "" && _cpu_id == "" {
		cmd_cpu_id := "wmic cpu get processorid"
		//cmdArgs := strings.Fields(cmd_cpu_id)
		//output, _ := exec.Command(cmdArgs[0], cmdArgs[1:len(cmdArgs)]...).Output()
		//outputStr := string(output)
		outputStr := ___windows_cmd(cmd_cpu_id)
		strArr := strings.Fields(outputStr)
		if len(strArr) == 2 {
			d.CPUID = strArr[1]
		}
		_cpu_id = d.CPUID
		//UUID
		cmd_uuid := "wmic csproduct get UUID"
		//cmdArgs = strings.Fields(cmd_uuid)
		//output, _ = exec.Command(cmdArgs[0], cmdArgs[1:len(cmdArgs)]...).Output()
		outputStr = ___windows_cmd(cmd_uuid)
		strArr = strings.Fields(outputStr)
		if len(strArr) == 2 {
			d.UUID = strArr[1]
		}
		_uuid = d.UUID
		if d.UUID == "" && d.CPUID == "" {
			err_str = "无法正常运行-101"
			return
		}
	} else {
		d.CPUID = _cpu_id
		d.UUID = _uuid
	}

	//加密数据
	code_str, isok := verifyauth.EncryptData(d)
	if !isok {
		err_str = "无法正常运行-102"
		return
	}
	code = code_str

	return
}
func ___windows_cmd(cmd_line string) (cmd_result_str string) {
	cmdArgs := strings.Fields(cmd_line)
	output, _ := exec.Command(cmdArgs[0], cmdArgs[1:len(cmdArgs)]...).Output()
	outputStr := string(output)
	return outputStr
}
