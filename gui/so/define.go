package so

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"os/exec"
	"seo_tools/gui"
	"strings"
)

type so struct {
	*gui.G
}

type T_SO struct {
	*so
}

//注册Sogou相关的
var _so *so

func Register(g *gui.G) {
	_so = &so{g}
	_so.UI.Bind("so_submit_1", _so.submit_1)
	_so.UI.Bind("so_pm_pc", _so.pm_pc)
	//_so.UI.Bind("so_pm_m", _so.)
}

//获取坐标
func (g *so) GETY() int {

	pycode := `
filename = '_file/360_ok.txt'



def s360good(txt ,filename=filename):
    with open(filename, 'w',encoding='utf8') as file_object:
        file_object.write(txt)
        file_object.close()

s360good('0')

def download_img(img_url, save_file_name):
    print(img_url)
    header = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36"}  # 设置http header，视情况加需要的条目，这里的token是用来鉴权的一种方式
    r = requests.get(img_url, headers=header, stream=True)
    print(r.status_code)  # 返回状态码
    if r.status_code == 200:
        open(save_file_name, 'wb').write(r.content)  # 将内容写入图片
        print("done")
    del r


def ResizeImage(filein, fileout, width, height, type):
    img = Image.open(filein)
    out = img.resize((width, height), Image.ANTIALIAS)
    # resize image with high-quality
    out.save(fileout, type)


#
# def generate_tracks(S):
#     """
#     :param S: 缺口距离Px
#     :return:
#     """
#     S += 20
#     v = 0
#     t = 0.2
#     forward_tracks = []
#     current = 0
#     mid = S * 3 / 5  # 减速阀值
#     while current < S:
#         if current < mid:
#             a = 2  # 加速度为+2
#         else:
#             a = -3  # 加速度-3
#         s = v * t + 0.5 * a * (t ** 2)
#         v = v + a * t
#         current += s
#         forward_tracks.append(round(s))
#
#     back_tracks = [-3, -3, -2, -2, -2, -2, -2, -1, -1, -1]
#     return {'forward_tracks': forward_tracks, 'back_tracks': back_tracks}

# def get_track(distance):
# track = []
# current = 0
# mid = distance * 3 / 4
# t = 0.2
# v = 0
# while current < distance:
#     if current < mid:
#         a = 2
#     else:
#         a = -3
#     v0 = v
#     v = v0 + a * t
#     dx = v0 * t + 1 / 2 * a * t * t
#
#     _tmp = current + dx
#     if _tmp > distance:
#         dx = _tmp - distance
#     current += dx
#     track.append(round(dx))
# return track

def generate_tracks(S):
    """
    :param S: 缺口距离Px
    :return:
    """
    S += 20
    v = 0
    t = 0.2
    forward_tracks = []
    current = 0
    mid = S * 3 / 5  # 减速阀值
    while current < S:
        if current < mid:
            a = 2  # 加速度为+2
        else:
            a = -3  # 加速度-3
        s = v * t + 0.5 * a * (t ** 2)
        v = v + a * t
        current += s
        forward_tracks.append(round(s))

    back_tracks = [-3, -3, -2, -2, -2, -2, -2, -1, -1, -1]
    return {'forward_tracks': forward_tracks, 'back_tracks': back_tracks}


def move_to_gap(browser, slider, track):
    '''
    拖动滑块到缺口处
    :param slider: 滑块
    :param track: 轨迹
    :return:
    '''
    currtnt = 0
    ActionChains(browser).click_and_hold(slider).perform()
    for x in track:
        currtnt += x
        ActionChains(browser).move_by_offset(xoffset=x, yoffset=0).perform()

    time.sleep(0.2)
    ActionChains(browser).release().perform()


def get_distance(bkg, blk):
    img = cv2.imread(blk, cv2.IMREAD_UNCHANGED)
    alpha_channel = img[:, :, 3]
    _, mask = cv2.threshold(alpha_channel, 254, 255, cv2.THRESH_BINARY)  # binarize mask
    color = img[:, :, :3]
    block = cv2.bitwise_not(cv2.bitwise_not(color, mask=mask))

    template = cv2.imread(bkg, 0)
    cv2.imwrite('_file/template.jpg', template)
    cv2.imwrite('_file/block.jpg', block)
    block = cv2.imread('_file/block.jpg')
    block = cv2.cvtColor(block, cv2.COLOR_BGR2GRAY)
    block = abs(255 - block)
    cv2.imwrite('_file/block.jpg', block)
    block = cv2.imread('_file/block.jpg')
    template = cv2.imread('_file/template.jpg')
    result = cv2.matchTemplate(block, template, cv2.TM_CCOEFF_NORMED)
    x, y = np.unravel_index(result.argmax(), result.shape)
    # 这里就是下图中的绿色框框
    # cv2.rectangle(template, (y, x), (y, x + 136-25), (7, 249, 151), 2)
    rectangle = cv2.rectangle(template, (y, x), (y, x + 120), (7, 249, 151), 2)
    cv2.imwrite('_file/template1.jpg', rectangle)
    # 之所以加20的原因是滑块的四周存在白色填充
    print(y)
    return int(y)


file_template = "_file/360_002.png"
file_template_smail = "_file/360_002_s.png"
file_block = "_file/360_001.png"
file_block_smail = "_file/360_001_s.png"

# 下载图片
driver = Chrome()
driver.get("http://info.so.com/site_submit.html")

time.sleep(3)
s_bock = driver.find_element_by_css_selector('.v-wrap-img-front').get_attribute("src")
download_img(s_bock, file_block)

print("下载 ", s_bock)
s_bock = driver.find_element_by_css_selector('.v-wrap-img-bg').get_attribute("src")
print("下载 ", s_bock)
download_img(s_bock, file_template)

ResizeImage(file_block, file_block_smail, 80, 198, "png")
ResizeImage(file_template, file_template_smail, 320, 198, "png")
y = get_distance(file_template_smail, file_block_smail)

# 调用py的浏览器驱动
if y == 0:
    os._exit(0)
# y = 100
tracks = generate_tracks(y)

# 滑动坐标
biaoji = driver.find_element_by_css_selector(".v-target-btn")
# move_to_gap(driver, biaoji, tracks["forward_tracks"])
time.sleep(1)
ActionChains(driver).drag_and_drop_by_offset(biaoji, xoffset=y, yoffset=0).perform()
# 检测验证码 是否通过
time.sleep(2)
# yzm_is_ok = driver.find_element_by_css_selector(".v-target-word").get_attribute("style")

# runjs = '''document.querySelector('.v-target-btn').style.left'''
yzm_is_ok = driver.find_element_by_css_selector('.v-target-btn').get_attribute('style')
if yzm_is_ok == 'left: 0px;':

    print("滑块验证码未通过")
    driver.close()
    os._exit(0)
print("滑动验证码通过")

input_data = {
    'site_type': '其他网站',
    'icp': '',
    'site': 'https://www.baidu.com',
    'email': '01651@qq.com',
}



runjs = '''
var select = document.querySelector("#select-type");

for (var i = 0; i< select.options.length;i++){
    if (select.options[i].value === "'''+input_data['site_type']+'''"){
        select.options[i].selected  = true;
        break
    }
}

//ICP
document.querySelector('#icp').value= "'''+input_data['icp']+'''";

//URL
document.querySelector('#url').value= "'''+input_data['site']+'''";

//邮箱
document.querySelector('#email').value= "'''+input_data['email']+'''";



'''
driver.execute_script(runjs)
time.sleep(1)
driver.find_element_by_css_selector('.bt-submit-0').click()
time.sleep(3)
text = driver.find_element_by_css_selector('h2').text
s360good(text)

driver.close()

`
	toString := base64.StdEncoding.EncodeToString([]byte(pycode))
	////toString := base64.RawStdEncoding.EncodeToString([]byte(pycode))
	//encode := c_code.Bs64Encode([]byte(pycode))
	//fmt.Println(toString,encode)
	strings.TrimSpace(___windows_cmd(fmt.Sprintf(".\\_exe\\help1.exe %s)", toString)))
	return 0
}

func ___windows_cmd(cmd_line string) (cmd_result_str string) {

	cmdArgs := strings.Fields(cmd_line)
	cmd, err := exec.Command(cmdArgs[0], cmdArgs[1:len(cmdArgs)]...).Output()
	fmt.Println(cmd, err)
	cmd_result_str = string(cmd)
	return
}

func Exec(name string, args ...string) error {
	cmd := exec.Command(name, args...)
	stderr, _ := cmd.StderrPipe()
	stdout, _ := cmd.StdoutPipe()
	if err := cmd.Start(); err != nil {
		log.Println("exec the cmd ", name, " failed")
		return err
	}
	// 正常日志
	logScan := bufio.NewScanner(stdout)
	go func() {
		for logScan.Scan() {
			log.Println(logScan.Text())
		}
	}()
	// 错误日志
	errBuf := bytes.NewBufferString("")
	scan := bufio.NewScanner(stderr)
	for scan.Scan() {
		s := scan.Text()
		log.Println("build error: ", s)
		errBuf.WriteString(s)
		errBuf.WriteString("\n")
	}
	// 等待命令执行完
	cmd.Wait()
	if !cmd.ProcessState.Success() {
		// 执行失败，返回错误信息
		return errors.New(errBuf.String())
	}
	return nil
}
