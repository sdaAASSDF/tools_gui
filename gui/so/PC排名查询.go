package so

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
	"log"
	"regexp"
	"seo_tools/kc"
	"strings"
	"time"
)

type pm_pc_s struct {
	CI     string `json:"ci"`
	Domain string `json:"domain"`
	Txt    string `json:"txt"`
	Stop   int    `json:"stop"`
}

func (g_ *so) pm_pc(_str string) {

	_f := pm_pc_s{}
	json.Unmarshal([]byte(_str), &_f)
	c := &kc.KcBody{
		TempDir: "c:\\suoha\\so\\pc",
		//T
		//TempDir = "c:\\click-temp\\"
	}
	c.Up()
	defer c.Off()
	chromedp.Run(c.Ctx, chromedp.Navigate("https://www.so.com"))
	chromedp.Run(c.Ctx,
		chromedp.SendKeys("#input", _f.CI, chromedp.ByQuery),
		chromedp.Click("#search-button", chromedp.ByQuery),
		chromedp.Sleep(time.Second*2),
	)
	max_page := _f.Stop
	if max_page == 0 {
		max_page = 10
	}
	g_.MessageClear()
	g_.UI.Eval("app.setf1pm('')")
	g_.Message("开始查询")
	start_page := 1
	//提取页面
	pm, next, text_list := g_.pm_pc_ok(c, _f.Domain, _f.Txt)
	for ttt_index, ttt := range text_list {
		pm_show := ttt_index + 1
		g_.Message(fmt.Sprintf("%d --> %s", pm_show, ttt))
	}

	if pm == 0 && next {
		for {
			g_.MessageError("暂停3秒")
			time.Sleep(time.Second * 3)
			//点击下一页
			chromedp.Run(c.Ctx, chromedp.Click(`#snext`, chromedp.ByQuery))
			pm, next, text_list = g_.pm_pc_ok(c, _f.Domain, _f.Txt)
			for ttt_index, ttt := range text_list {
				pm_show := start_page*10 + ttt_index + 1
				g_.Message(fmt.Sprintf("%d --> %s", pm_show, ttt))
			}

			if pm != 0 {
				pm = start_page*10 + pm
				break
			}
			if !next {
				break
			}
			if start_page >= max_page {
				break
			}
			start_page++
		}
	}
	if !next {
		g_.MessageError("没有下一页,执行退出操作")
	}
	if pm == 0 {
		g_.MessageError("未找到排名")
	} else {
		g_.Message("你的排名为 " + fmt.Sprintf("%d", pm))
		g_.UI.Eval("app.setf1pm(" + fmt.Sprintf("%d", pm) + ")")
	}

}

func (g_ *so) pm_pc_ok(c *kc.KcBody, domain, txt string) (pm int, next bool, l []string) {

	var doc *goquery.Document
	var err error
	//检测是否出现验证码
	for {
		time.Sleep(time.Second)
		html := c.Html(c.Ctx)
		if html == "" {
			g_.MessageError("获取HTML失败，直接退出")
			return
		}
		if regexp.MustCompile(`系统检测`).MatchString(html) {
			g_.MessageError("出现验证码等待处理")
			time.Sleep(time.Second)
			continue
		}

		doc, err = goquery.NewDocumentFromReader(strings.NewReader(html))
		if err != nil {
			log.Println(err)
			return
		}
		defer doc.Clone()

		break
	}

	if doc.Find(".result .res-list").Size() == 0 {
		return
	}
	is_ok := false
	doc.Find(".result .res-list").Each(func(i int, selection *goquery.Selection) {
		//if is_ok {
		//	return
		//}
		selection.Find("style").Remove()
		text := selection.Find(".res-linkinfo cite").Text()
		if text == "" {
			text = selection.Find(".url").Text()
		}
		l = append(l, text)
		if strings.Contains(text, domain) && !is_ok && domain != "" {
			pm = i + 1
			is_ok = true
			return
		}
		if strings.Contains(text, txt) && !is_ok && txt != "" {
			pm = i + 1
			is_ok = true
			return
		}
	})
	//检测是否存在下一页
	next_text := doc.Find("#snext").Text()
	next = strings.Contains(next_text, "下一页")
	return
}
