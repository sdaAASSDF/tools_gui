package so

import (
	"encoding/base64"
	"fmt"
	"log"
	"strings"
)

//input_data = {
//'site_type': '其他网站',
//'icp': '',
//'site': 'https://www.baidu.com',
//'email': '01651@qq.com',
//}

type _pycodes struct {
	SiteType string
	ICP      string
	SITE     string
	Email    string
}

func pycode() string {
	py := `

def ResizeImage(filein, fileout, width, height, type):
  img = Image.open(filein)
  out = img.resize((width, height),Image.ANTIALIAS)
  #resize image with high-quality
  out.save(fileout, type)

def transparence2white(img):
    sp=img.shape  # 获取图片维度
    width=sp[0]  # 宽度
    height=sp[1]  # 高度
    for yh in range(height):
        for xw in range(width):
            color_d=img[xw,yh]  # 遍历图像每一个点，获取到每个点4通道的颜色数据
            if(color_d[3]==0):  # 最后一个通道为透明度，如果其值为0，即图像是透明
                img[xw,yh]=[255,255,255,255]  # 则将当前点的颜色设置为白色，且图像设置为不透明
    return img


def get_distance(bkg,blk):
    img = cv2.imread(blk, cv2.IMREAD_UNCHANGED)
    # cv2.imshow(img)
    # cv2.waitKey(0)
    # alpha_channel = img[:, :, 3]
    # _, mask = cv2.threshold(alpha_channel, 255, 255, cv2.THRESH_BINARY)  # binarize mask
    # color = img[:, :, :3]
    # block = cv2.bitwise_not(cv2.bitwise_not(color, mask=mask))
    block = transparence2white(img)

    template = cv2.imread(bkg, 0)
    cv2.imwrite('_file/template.jpg', template)
    cv2.imwrite('_file/block.jpg', block)
    block = cv2.imread('_file/block.jpg')
    block = cv2.cvtColor(block, cv2.COLOR_BGR2GRAY)
    block = abs(255 - block)
    cv2.imwrite('_file/block.jpg', block)
    block = cv2.imread('_file/block.jpg')
    template = cv2.imread('_file/template.jpg')
    result = cv2.matchTemplate(block,template,cv2.TM_CCOEFF_NORMED)
    x, y = np.unravel_index(result.argmax(),result.shape)
    #这里就是下图中的绿色框框
    # cv2.rectangle(template, (y, x), (y, x + 136-25), (7, 249, 151), 2)
    rectangle = cv2.rectangle(template, (y, x), (y, x + 120), (7, 249, 151), 2)
    cv2.imwrite('_file/template1.jpg', rectangle)
    #之所以加20的原因是滑块的四周存在白色填充
    print(y)

file_template = "_file/360_002.png"
file_template_smail = "_file/360_002_s.png"
file_block = "_file/360_001.png"
file_block_smail = "_file/360_001_s.png"
ResizeImage(file_block,file_block_smail,80,198,"png")
ResizeImage(file_template,file_template_smail,320,198,"png")
get_distance(file_template_smail,file_block_smail)

`

	toString := base64.StdEncoding.EncodeToString([]byte(py))
	////toString := base64.RawStdEncoding.EncodeToString([]byte(pycode))
	//encode := c_code.Bs64Encode([]byte(pycode))
	//fmt.Println(toString,encode)
	_str := strings.TrimSpace(___windows_cmd(fmt.Sprintf(".\\_exe\\help1.exe %s)", toString)))
	log.Println("py cmd", _str)
	return _str

}
