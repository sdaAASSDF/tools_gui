package so

import (
	"context"
	"fmt"
	"github.com/123456/c_code"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/input"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
	"github.com/disintegration/imaging"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"regexp"
	"seo_tools/filemd5"
	"seo_tools/gui"
	"seo_tools/kc"
	"strconv"
	"strings"
	"time"
)

func (g_ *so) submit_1(data string) {
	g_.MessageClear()
	//验证指纹
	if !filemd5.Help1OK() {
		g_.MessageError("<-- 大佬别搞我 -->")
		return
	}
	list := []_pycodes{}
	for _, v := range strings.Split(strings.ReplaceAll(data, "\r", ""), "\n") {
		v = strings.TrimSpace(v)
		if v == "" {
			continue
		}
		s_data := strings.Split(v, "|")
		if len(s_data) != 4 {
			continue
		}
		list = append(list, _pycodes{
			SiteType: strings.TrimSpace(s_data[0]),
			ICP:      strings.TrimSpace(s_data[1]),
			SITE:     strings.TrimSpace(s_data[2]),
			Email:    strings.TrimSpace(s_data[3]),
		})
	}
	if len(list) == 0 {
		g_.MessageError("请填写需要提交的数据")
		return
	}
	for _, py := range list {
		for {
			if g_.submit_1_one(py) {
				break
			}
		}
		time.Sleep(time.Second * 3)
		g_.MessageError(fmt.Sprintf("暂停 3 秒执行下个提交"))
	}
	g_.Message("所有链接提交完成")
}

func (g_ *so) submit_1_one(py _pycodes) (isok bool) {
	g_.Message(fmt.Sprintf("开始提交 %s", py.SITE))
	c := &kc.KcBody{
		UA:      "",
		TempDir: "",
	}
	c.Up()
	//watch_img(c.Ctx)
	defer c.Close()
	if err := chromedp.Run(c.CtxTimeOutCtx(time.Second*30), network.Enable(), chromedp.Navigate("http://info.so.360.cn/site_submit.html")); err != nil {
		g_.MessageError(fmt.Sprintf("打开推送接口失败"))
		return
	}
	time.Sleep(time.Millisecond * 1500)
	//尝试获取图片
	is_img_ok := c.GetIMG(".v-wrap-img-front", gui.S360Block)
	if !is_img_ok {
		return
	}

	is_img_ok = c.GetIMG(".v-wrap-img-bg", gui.S360Template)
	if !is_img_ok {
		return
	}

	//获取坐标
	code := pycode()
	yyyy, _ := strconv.Atoi(code)

	if yyyy == 0 {
		g_.MessageError(fmt.Sprintf("获取坐标失败"))
		return
	}
	//获取坐标
	nodes := []*cdp.Node{}

	chromedp.Run(c.Ctx, chromedp.Nodes(".v-target-btn", &nodes, chromedp.ByQuery))
	//MouseNodePressed(nodes[0])

	ssszuobiao := chromedp.SSSZUOBIAO{}
	chromedp.Run(c.Ctx,
		chromedp.GETNODEXY(nodes[0], &ssszuobiao, chromedp.ButtonType(input.Left)),
	)

	err := chromedp.Run(c.CtxTimeOutCtx(time.Second*10),
		chromedp.MouseEvent(input.MousePressed, ssszuobiao.X, ssszuobiao.Y, chromedp.ButtonType(input.Left)),
		chromedp.MouseEvent(input.MouseMoved, ssszuobiao.X+float64(yyyy), ssszuobiao.Y),
		chromedp.MouseEvent(input.MouseReleased, ssszuobiao.X+float64(yyyy), ssszuobiao.Y, chromedp.ButtonType(input.Left)),
	)
	if err != nil {
		g_.MessageError(fmt.Sprintf("移动坐标失败"))
		return
	}

	//检测是否通过
	time.Sleep(time.Second * 2)
	html := ""
	err = chromedp.Run(c.Ctx, chromedp.OuterHTML(".v-target-btn", &html, chromedp.ByQuery))
	if err != nil {
		return
	}
	//<span class="v-target-btn" style="left: 206px;"></span>

	submatch := regexp.MustCompile(`left: (\d{1,8})px`).FindStringSubmatch(html)
	if len(submatch) != 2 {
		g_.MessageError(fmt.Sprintf("坐标未通过"))
		return
	}
	if submatch[1] == "0" {
		g_.MessageError(fmt.Sprintf("坐标未通过"))
		return
	}
	g_.Message(fmt.Sprintf("坐标通过 开始填写信息"))
	if py.Email == "" {
		py.Email = c_code.RandEmail(c_code.RandDom(5, 12))
	}
	runjs := `
		var select = document.querySelector("#select-type");

		for (var i = 0; i< select.options.length;i++){
			if (select.options[i].value === "` + py.SiteType + `"){
				select.options[i].selected  = true;
					break
				}
			}

				//ICP
				document.querySelector('#icp').value= "` + py.ICP + `";

				//URL
				document.querySelector('#url').value= "` + py.SITE + `";

				//邮箱
				document.querySelector('#email').value= "` + py.Email + `";

				`
	h := ""
	chromedp.Run(c.CtxTimeOutCtx(time.Second*5),
		chromedp.Evaluate(runjs, &h))
	time.Sleep(time.Millisecond * 300)
	chromedp.Run(c.Ctx, chromedp.Click(".bt-submit-0", chromedp.ByQuery))

	for i := 10; i < 10; i++ {
		time.Sleep(time.Millisecond * 100)
		loca := ""
		chromedp.Run(c.Ctx, chromedp.Location(&loca))
		if loca != "http://info.so.360.cn/site_submit.html" {
			break
		}
	}
	//获取文字信息
	res := ""
	chromedp.Run(c.Ctx, chromedp.Text("h2", &res))
	g_.Message(fmt.Sprintf("---> %s", res))
	return true
}

func watch_img(ctx context.Context) {
	chromedp.ListenTarget(ctx, func(ev interface{}) {
		switch ev := ev.(type) {
		case *network.EventResponseReceived:
			resp := ev.Response
			if len(resp.Headers) != 0 {
				r_url, e := url.QueryUnescape(resp.URL)
				if e != nil {
					return
				}
				if _, ok := resp.Headers["Content-Type"]; ok {
					//下载图片
					p_url, e := url.Parse(r_url)
					if e != nil {
						return
					}
					content_type := strings.ToLower(resp.Headers["Content-Type"].(string))
					if strings.Contains(content_type, "image") {

						if strings.HasPrefix(p_url.Path, "/t") && p_url.Host == "p3.qhimg.com" {
							fmt.Println(p_url, e, resp.Headers["Content-Type"])
							go func() {
								if err := chromedp.Run(ctx, chromedp.ActionFunc(func(ctx context.Context) error {
									file_name := "_file/tmp_" + c_code.Md532(time.Now().String()) + fmt.Sprintf("%d.png", c_code.RandDom(0, 50000))
									body, err := network.GetResponseBody(ev.RequestID).Do(ctx)
									if err != nil {
										return err
									}
									defer os.Remove(file_name)
									err = ioutil.WriteFile(file_name, body, os.ModePerm)
									if err != nil {
										log.Println(err)
										return err
									}
									img_src, err := imaging.Open(file_name)
									if err != nil {
										return err
									}
									defer imaging.Clone(img_src)

									if img_src.Bounds().Size().X == 146 {
										imaging.Save(img_src, gui.S360Block)
									}
									if img_src.Bounds().Size().X == 580 {
										imaging.Save(img_src, gui.S360Block)
									}

									//respBody <- string(body)
									//return err
									return err
								})); err != nil {
									return
								}
							}()
						}
					}
				}
			}

		}
	})
}
