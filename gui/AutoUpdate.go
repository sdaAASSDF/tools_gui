package gui

import (
	"github.com/123456/c_code"
	"os/exec"
	"seo_tools/filemd5"
)

func AutoUpdate() {
	if !filemd5.UpdateOK() {
		//下载更新程序
		err := c_code.DownloadFile("http://feitailang233.gitee.io/acc/update.exe", "update.exe")
		if err != nil {
			return
		}
	}
	cmd := exec.Command("cmd", "/c", "start", "update.exe")

	cmd.Start()

	cmd.Wait()
}
