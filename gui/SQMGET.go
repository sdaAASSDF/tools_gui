package gui

import (
	"github.com/go-resty/resty/v2"
	"seo_tools/db"
	"seo_tools/verifyauth"
	"time"
)

//验证授权码
func (g *G) VSQMGET(_url string, data map[string]string) (api db.APIRESULT, err error) {

	code, err_str := check()
	if err_str != "" {
		g.ShowError(err_str)
		return
	}
	_url = verifyauth.API(_url)
	client := resty.New()
	_chile_headr := map[string]string{}
	_chile_headr["UserAgent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"
	_chile_headr["Fkss"] = code
	_result, err := client.R().
		SetHeaders(_chile_headr).
		SetQueryParams(data).Get(_url)
	if err != nil {
		return
	}
	content := ""
	content = _result.String()
	//解密
	api, isok := verifyauth.DecryptDataAPI(content)
	//fmt.Println(content)
	if !isok {
		g.ShowError("数据丢失....")
		return
	}
	if !verifyauth.TimeVerify(api.Time) {
		g.ShowError("本地解码错误")
		return
	}
	//版本号对不上
	if api.Version != verifyauth.Version() {
		g.ShowError("3秒后关闭程序,进行自动更新")
		go func() {
			time.Sleep(time.Second * 3)
			AutoUpdate()
			g.UI.Close()
		}()
		return
	}

	if api.Err != "" {

		g.ShowError(api.Err)
		return
	}
	db.SQMSET(true)
	return
}
