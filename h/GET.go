package h

import (
	"github.com/go-resty/resty/v2"
)

func Get(_url string, data map[string]string, cookie string) (content string, err error) {
	//_url = "http://127.0.0.1:7820/" + strings.TrimLeft(_url, "/")

	client := resty.New()
	_chile_headr := map[string]string{}
	_chile_headr["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"
	if cookie != "" {
		_chile_headr["Cookie"] = cookie
	}

	_result, err := client.R().
		SetHeaders(_chile_headr).
		SetQueryParams(data).Get(_url)
	if err != nil {
		return
	}
	content = _result.String()
	return
}
