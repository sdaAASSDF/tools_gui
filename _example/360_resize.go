package main

import (
	"github.com/disintegration/imaging"
	"io/ioutil"
	"strings"
)

func main() {
	dir, _ := ioutil.ReadDir("_example/360")
	for _, v := range dir {
		split := strings.Split(v.Name(), "_")
		if len(split) != 2 {

			continue
		}
		file_name := strings.ToLower(split[0]) + "_123" + split[1]
		src, err := imaging.Open("_example/360/" + v.Name())
		if err != nil {

			continue
		}

		//imaging.C
		src = imaging.CropAnchor(src, 50, src.Bounds().Size().X, imaging.Left)
		err = imaging.Save(src, "_example/360_2/"+file_name)
		//os.Rename("_example/360/"+v.Name(),"_example/360/"+file_name)
	}
}
