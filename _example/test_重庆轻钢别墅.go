package main

import (
	"fmt"
	"github.com/123456/c_code"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	file, _ := ioutil.ReadFile("_example/1.txt")
	sliceString := c_code.UniqueSliceString(strings.Split(strings.ReplaceAll(string(file), "\r", ""), "\n"))
	list := []string{}
	for _, v := range sliceString {
		if v == "" {
			continue
		}
		list = append(list, fmt.Sprintf("企业官网|渝ICP备20004727号|%s|748817344@qq.com", v))
	}
	ioutil.WriteFile("_example/1_1.txt", []byte(strings.Join(list, "\n")), os.ModePerm)
}
