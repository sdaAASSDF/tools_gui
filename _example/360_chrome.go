package main

import (
	"context"
	"fmt"
	"github.com/123456/c_code"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"seo_tools/dama/chaojiyin"
	"seo_tools/kc"
	"strings"
	"time"
)

func main() {
	for {
		submit_360()
	}

}

func submit_360() {
	c := &kc.KcBody{
		UA:      "",
		TempDir: "c:\\suoha\\seo\\360",
	}
	c.Up()
	defer c.Close()
	c2 := make(chan string, 1)
	down_captcha(c.Ctx, c2)
	chromedp.Run(c.Ctx,
		network.Enable(),
		c.SetCookieNavigate("http://zhanzhang.so.com/sitetool/page_include", "test_cookie_enable=null; QiHooGUID=AA4F1F0E45CD878444E98F2BE9E9DC26.1592930610680; __guid=15484592.3878029364647752000.1592930610106.7078; __huid=11Q9wFaNvpR2ExVKkGbh7H5k8pWoRcOaM7q%2BPF%2FVe9YqU%3D; __gid=9114931.128507831.1594381696639.1594381697558.14; PHPSESSID=hjn48udgfg9l5hnb1bg89m1607; Qs_lvt_100433=1594793513%2C1594816887%2C1594861518; test_cookie_enable=null; Q=u%3Dfqsfre23fnq%26n%3D%26le%3DAGpkAGxmZQNyAQOkpF5wo20%3D%26m%3DZGt5WGWOWGWOWGWOWGWOWGWOZwZ3%26qid%3D3212212524%26im%3D1_t011655040b3ed000bf%26src%3Dpcw_so_zhanzhang%26t%3D1; T=s%3D192b0e36d6fae2efe0de2a9c5e7d03c7%26t%3D1594869583%26lm%3D%26lf%3D1%26sk%3D58b3e8b4cd34eb0536fb62891cff7356%26mt%3D1594869583%26rc%3D%26v%3D2.0%26a%3D1; Qs_pv_100433=1450853534747267300%2C2824792445153748000%2C847778554018540500%2C3874787433139927600%2C221905360759768200"))
	//chromedp.Run(c.Ctx,network.Enable())
	for i := 1; i <= 5; i++ {
		err := chromedp.Run(c.CtxTimeOutCtx(time.Second*3), chromedp.SendKeys("#zz-input-"+fmt.Sprintf("%d", i), "https://www.baidu.com", chromedp.ByID))
		if err != nil {
			log.Println(err)
			return
		}
	}
	c_code.CopyFile("_example/360_2.png", "_example/360.png")
	chaojiying := chaojiyin.Chaojiying{}
	chaojiying.InitWithOptions()
	val := chaojiying.GetPicVal("fbavasfvafva", "fhfADer345", "625c09ef4570d7812b68a3bdf19cea34", "1004", "0", "_example/360.png")
	result := string(val)
	code := gjson.Get(result, "pic_str").String()
	if len(code) != 4 {
		log.Println("验证码识别失败")
		return
	}
	err := chromedp.Run(c.Ctx,
		chromedp.SendKeys(`input[name="checkcode"]`, code),
	)
	if err != nil {
		log.Println(err)
		return
	}
	time.Sleep(time.Millisecond * 300)
	err = chromedp.Run(c.Ctx, network.Enable(), chromedp.Click(".zz-button.page_include-submit-btn", chromedp.ByQuery))
	if err != nil {
		log.Println(err)
		return
	}
	data := <-c2
	message := gjson.Get(data, "info").String()
	result_code := gjson.Get(data, "status").Int()
	if strings.Contains(message, "验证码") {
		log.Println("验证码提交失败")
		return
	}

	c_code.CopyFile("_example/360/"+code+"_"+c_code.Md532(time.Now().String())+".png", "_example/360_2.png")
	log.Println(message, result_code)
}

func down_captcha(ctx context.Context, logninok chan (string)) {
	chromedp.ListenTarget(ctx, func(ev interface{}) {
		switch ev := ev.(type) {
		case *network.EventResponseReceived:
			resp := ev.Response
			if len(resp.Headers) != 0 {
				r_url, e := url.QueryUnescape(resp.URL)
				if e != nil {
					return
				}
				if _, ok := resp.Headers["Content-Type"]; ok {
					//fmt.Println(resp.Headers["Content-Type"])
					//下载图片
					p_url, e := url.Parse(r_url)
					if e != nil {
						return
					}
					content_type := strings.ToLower(resp.Headers["Content-Type"].(string))
					if strings.Contains(content_type, "image") {
						//fmt.Println(p_url, e, resp.Headers["Content-Type"])
						if strings.HasPrefix(p_url.Path, "/index.php") && p_url.Query().Get("a") == "checkcode" && p_url.Query().Get("m") == "Utils" {
							go func() {
								if err := chromedp.Run(ctx, chromedp.ActionFunc(func(ctx context.Context) error {
									body, err := network.GetResponseBody(ev.RequestID).Do(ctx)
									if err != nil {

										return err
									}
									err = ioutil.WriteFile("_example/360.png", body, os.ModePerm)

									//respBody <- string(body)
									//return err
									return err
								})); err != nil {
									return
								}
							}()
						}

					}

					//监听AJAX请求登录请求
					if strings.Contains(content_type, "application/json") && p_url.Query().Get("m") == "PageInclude" && p_url.Query().Get("a") == "upload" {
						go func() {
							if err := chromedp.Run(ctx, chromedp.ActionFunc(func(ctx context.Context) error {
								body, err := network.GetResponseBody(ev.RequestID).Do(ctx)
								if err != nil {
									return err
								}
								logninok <- string(body)
								return err
							})); err != nil {
								return
							}
						}()
					}
				}
			}

		}
	})
}
