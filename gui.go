package main

import (
	"fmt"
	"github.com/123456/c_code"
	"github.com/zserge/lorca"
	"log"
	"os"
	"runtime"
	"seo_tools/db"
	"seo_tools/gui"
	"seo_tools/gui/baidu"
	"seo_tools/gui/so"
	"seo_tools/gui/sogou"
	"seo_tools/server"
)

func main() {
	db.SetDebug(true)
	//db.SetDebug(false)
	os.Mkdir("_file/", os.ModePerm)
	//获取授权码
	db.Init()
	defer db.Close()
	port := c_code.GetRandomPort()
	go func(port int) {
		server.Server(port)
	}(port)
	args := []string{}
	if runtime.GOOS == "linux" {
		args = append(args, "--class=Lorca")
	}
	ui, err := lorca.New("", "", 1080, 780, args...)
	if err != nil {
		log.Fatal(err)
	}
	defer ui.Close()
	g := gui.G{
		UI: ui,
	}
	ui.Load(fmt.Sprintf("http://127.0.0.1:%d", port))

	//验证授权
	_, err = g.VSQMGET("/i_ai_suoha", map[string]string{})
	if err != nil {
		ui.Close()
		log.Fatal()
	}
	//os.RemoveAll()
	//绑定提示信息
	ui.Bind("ms", g.Message)
	ui.Bind("ms_err", g.MessageError)
	ui.Bind("ms_clear", g.MessageClear)

	//绑定事件
	ui.Bind("loginsm", g.TJSMGetCookie)
	ui.Bind("loginsmset", g.TJSMSetCookie)
	ui.Bind("sm_change_domain", g.TJSMChangeDomain)
	ui.Bind("sm_submit", g.TJSMSubmit)
	ui.Bind("sm_cancel", g.TJSMCancel)
	ui.Bind("sqm_update", g.SQMSET)

	//注册搜狗所有的相关操作
	sogou.Register(&g)

	//注册360搜索的方法
	so.Register(&g)

	//注册百度相关得方法
	baidu.Register(&g)

	//sigc := make(chan os.Signal)
	//signal.Notify(sigc, os.Interrupt)
	select {
	//case <-sigc:
	case <-ui.Done():
	}

	log.Println("exiting...")

}
