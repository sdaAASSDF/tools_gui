package main

import (
	"fmt"
	"github.com/123456/c_code"
	"io/ioutil"
	"os"
	"path"
	"strings"
)

//e0b8e927bae625d31e7b413657731048
//9769575260a8ab903f9239da6ebc11a3
func main() {
	//file, err := ioutil.ReadFile("update.exe")
	//if err != nil {
	//	return
	//}
	//md532 := c_code.Md532(string(file))
	//fmt.Println(md532)

	files := []string{"update.exe", "_exe/chromedriver.exe", "_exe/help1.exe"}

	os.RemoveAll("filemd5")
	os.Mkdir("filemd5", os.ModePerm)

	f_list := []map[string]string{}

	for _, v := range files {
		file, err := ioutil.ReadFile(v)
		if err != nil {
			return
		}

		upper := c_code.Capitalize(strings.ReplaceAll(path.Base(v), ".exe", ""))
		md532 := c_code.Md532(string(file))
		go_code := fmt.Sprintf(`
package filemd5

import (
	"github.com/123456/c_code"
	"io/ioutil"
)

func %s()  string { 
 return "%s"
}

func %sLoca()  string { 
file, err := ioutil.ReadFile("%s")
 if err != nil {
		return ""
	}
	md532 := c_code.Md532(string(file))
	return md532
}


func %sOK () bool {
	if (%s() == %sLoca()){
	return true
	}
return false
}

`, strings.ToLower(upper), md532, strings.ToLower(upper), v, upper, strings.ToLower(upper), strings.ToLower(upper))

		f_list = append(f_list, map[string]string{
			"f":    "filemd5/" + upper + ".go",
			"code": go_code,
		})
	}

	for _, f := range f_list {
		ioutil.WriteFile(f["f"], []byte(f["code"]), os.ModePerm)
	}
}
