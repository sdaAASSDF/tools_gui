package sogou_captcha

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"github.com/123456/c_code"
	"go.zoe.im/surferua"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/cookiejar"
	"os"
	"os/exec"
	"regexp"
	"runtime"
	"seo_tools/gui"
	"strings"
	"time"
)

func DownLoad() {

	//使用https，设置不验证
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	jar, _ := cookiejar.New(nil)
	http_client := &http.Client{
		Transport: tr,
		Jar:       jar,
	}

	http_client.Timeout = 1 * time.Minute
	se := &gui.Self{
		Client: http_client,
		Ua:     surferua.New().Windows().String(),
	}
	//进入页面
	if !se.SoGouView() {
		return
	}
	//拉取验证码
	if !se.SoGouGetCaptcha() {
		return
	}
	//进行二值化
	if !ExecPyImg() {
		return
	}
	code := AIQQGetCode()
	if code == "" {
		log.Println("提取验证码失败")
		return
	}
	if se.SoGouSubmit(code) {
		hash := c_code.Md532(fmt.Sprintf("%d", time.Now().UnixNano()))
		_, err := c_code.CopyFile("s/"+code+"_"+hash+".png", "sogou.png")
		if err != nil {
			log.Println("复制文件失败")
		}
		_, err = c_code.CopyFile("s/"+code+"_"+"old"+hash+".png", "sogou_old.png")
		if err != nil {
			log.Println("复制文件失败")
		}
	}
}

func ExecPyImg() bool {
	fmt.Println(runtime.GOOS)

	if strings.HasPrefix(runtime.GOOS, "window") {
		cmd := exec.Command("cmd.exe", "/c", "1.exe")
		err := cmd.Run()
		if err != nil {
			log.Println("启动失败:", err)
			return false
		} else {
			log.Println("启动成功!")
		}

		cmd.Wait()
		return true
	} else {
		cmd := exec.Command("/bin/bash", "-c", "./1")
		err := cmd.Run()
		if err != nil {
			log.Println("启动失败:", err)
			return false
		} else {
			log.Println("启动成功!")
		}

		cmd.Wait()
		return true
	}
	return false
}

func AIQQGetCode() string {
	file, err := postFile("sogou.png", "https://ai.qq.com/cgi-bin/appdemo_generalocr?g_tk=959893323")
	if err != nil {
		log.Println(err)
		return ""
	}
	defer file.Body.Close()
	all, err := ioutil.ReadAll(file.Body)
	if err != nil {
		log.Println(err)
		return ""
	}
	code := ""
	findString := regexp.MustCompile(`itemstring.*"(.*?)"`).FindStringSubmatch(string(all))
	if len(findString) == 2 {
		code = findString[1]
	}
	return code
}
func postFile(filename string, target_url string) (*http.Response, error) {
	body_buf := bytes.NewBufferString("")
	body_writer := multipart.NewWriter(body_buf)

	// use the body_writer to write the Part headers to the buffer
	_, err := body_writer.CreateFormFile("image_file", filename)
	if err != nil {
		fmt.Println("error writing to buffer")
		return nil, err
	}

	// the file data will be the second part of the body
	fh, err := os.Open(filename)
	if err != nil {
		fmt.Println("error opening file", err.Error())
		return nil, err
	}
	defer fh.Close()
	// need to know the boundary to properly close the part myself.
	boundary := body_writer.Boundary()
	//close_string := fmt.Sprintf("\r\n--%s--\r\n", boundary)
	close_buf := bytes.NewBufferString(fmt.Sprintf("\r\n--%s--\r\n", boundary))

	// use multi-reader to defer the reading of the file data until
	// writing to the socket buffer.
	request_reader := io.MultiReader(body_buf, fh, close_buf)
	fi, err := fh.Stat()
	if err != nil {
		fmt.Printf("Error Stating file: %s", filename)
		return nil, err
	}
	req, err := http.NewRequest("POST", target_url, request_reader)
	if err != nil {
		return nil, err
	}

	// Set headers for multipart, and Content Length
	req.Header.Add("Content-Type", "multipart/form-data; boundary="+boundary)
	req.ContentLength = fi.Size() + int64(body_buf.Len()) + int64(close_buf.Len())

	return http.DefaultClient.Do(req)
}
